��    G      T  a   �                -  
   H     S  	   V     `     o           �     �     �  
   �     �  m  �     L     _     t  8   �     �     �     �     �     	     
	     	     	     ;	  E   C	  W   �	     �	     �	      
     
     '
  
   ,
  
   7
     B
     P
  %   _
     �
     �
     �
     �
     �
     �
     �
     �
  F   �
     ?     V     c     r     �     �     �  \   �       L        _     m  =   �     �  T   �     )     ;  &   Y     �     �     �     �  P  �  8   +  7   d     �     �     �     �     �  <        J     h     �  
   �     �  �  �  0   �  *   �       ?   #  G   c     �     �     �                     /     2  �   G  �   �     b  '   w     �  )   �     �      �             +   ,  +   X     �     �     �     �     �  "   �     �  !     y   3  %   �     �     �     	     %     C  (   T  �   }       v   "     �     �  o   �  *   >  g   i  *   �     �  &        A  =   X     �     �                  /   F   2          ?              6          *   !   <   .   1   5   #   E         :      "           (   ;       %   A           $      7   +               -      )      @          D                         0                   8         9   >         B      &   	       =   C       
           4          3      '           G   ,               Please activate %1$s. %2$s  Please install %1$s. %2$s % Comments ,  1 Comment 1 Sidebar Left 1 Sidebar Right 1 Sidebar Right & 1 Sidebar Left 2 Sidebars Left 2 Sidebars Right Activate Automattic Colinear Colinear -- our update to the older Coraline -- is a squeaky-clean theme featuring a custom menu, header, background, and layout options. Colinear supports featured images and six widget areas -- up to three each in the sidebar and footer. Primarily designed for magazine-style sites, Colinear is a flexible theme that suits any personal blog, or content-rich site. Comment navigation Comments are closed. Continue reading %s Continue reading %s <span class="meta-nav">&rarr;</span> Custom Content Types module Edit Featured Post Featured Sidebar Footer 1 Footer 2 Footer 3 Inconsolata font: on or offon Install It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Jetpack plugin Leave a comment Lora font: on or offon Newer Comments Next Next post: No Sidebar Nothing Found Older Comments Oops! That page can&rsquo;t be found. Open Sans font: on or offon Page Pages: Posted in %1$s Previous Previous post: Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Sidebar Left Sidebar Left 2 Sidebar Right Sidebar Right 2 Sidebars Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Tagged %1$s The %1$s is required to use some of this theme&rsquo;s features, including:  Theme Options Theme: %1$s by %2$s. To use %1$s, please activate the Jetpack plugin&rsquo;s %2$s. collapse child menu comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; expand child menu https://wordpress.com/themes/ https://wordpress.com/themes/colinear/ https://wordpress.org/ no specific module needed post authorby %s post datePosted on %s PO-Revision-Date: 2017-07-22 12:25:27+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: ru
Project-Id-Version: Themes - Colinear
  Пожалуйста, активируйте %1$s. %2$s   Пожалуйста, установите %1$s. %2$s Комментарии (%) ,  1 Комментарий 1 сайдбар слева 1 сайдбар справа 1 Сайдбар справа и 1 сайдбар слева 2 сайдбара слева 2 сайдбара справа Активировать Automattic Colinear Colinear -- наше обновление для старой Coraline -- безупречная тема с пользовательским меню, заголовком, фоном и вариантами макета. Colinear поддерживает изображения записей и включает шесть областей виджетов -- до трех в боковом и нижнем колонтитулах. Colinear, в первую очередь предназначенная для сайтов в стиле журнала, -- это гибкая тема, которая подходит для любого личного блога или сайта с богатым информационным наполнением. Навигация по комментариям Комментарии запрещены. Читать далее %s Читать далее %s <span class="meta-nav">&rarr;</span> Модуль пользовательских типов записей Редактировать Избранная запись Особый сайдбар Подвал 1 Подвал 2 Подвал 3 on Установить По данному адресу ничего не найдено. Попробуйте воспользоваться поиском. Запрошенную информацию найти не удалось. Возможно, будет полезен поиск по сайту. Плагин Jetpack Оставить комментарий on Следующие комментарии Следующая Следующая запись: Без сайдбара Не найдено Предыдущие комментарии Ой! Страница не найдена. on Страница Страницы: Рубрики: %1$s Предыдущая Предыдущая запись: Основное меню Сайт работает на %s Готовы опубликовать свою первую запись? <a href="%1$s">Начните отсюда</a>. Результаты поиска: %s Сайдбар слева Сайдбар слева 2 Сайдбар справа Сайдбар справа 2 Сайдбары Перейти к содержимому Извините, по вашему запросу ничего не найдено. Попробуйте другие ключевые слова. Метки: %1$s %1$s необходим для использования некоторых функций темы, включая:  Настройки темы Тема %1$s от %2$s. Для использования %1$s, пожалуйста, активируйте плагин Jetpack %2$s. свернуть дочернее меню %2$s: %1$s комментарий %2$s: %1$s комментария %2$s: %1$s комментариев раскрыть дочернее меню https://wordpress.com/themes/ https://wordpress.com/themes/colinear/ https://wordpress.org/ определенный модуль не требуется От %s Опубликовано %s 