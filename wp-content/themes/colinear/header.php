<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Colinear
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'colinear' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="header-info-container">
			<div class="site-branding">
				<?php colinear_the_site_logo(); ?>

				<?php
					if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php endif;

					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; ?></p>
					<?php endif;
				?>
			</div><!-- .site-branding -->

			<div class="site-language">
				<button class="language-toggle russian"><img src="<?php echo esc_url( get_template_directory_uri(), 'colinear' ) ?>/icons/russian.png" class="rus-toggle-img"><p class="text">Рус</p></button>
				<button class="language-toggle english"><img src="<?php echo esc_url( get_template_directory_uri(), 'colinear' ) ?>/icons/english.png" class="en-toggle-img><p class="text">En</p></button>
			</div><!-- .site-language-->

			<div class="site-contacts">
				<button class="buy-canvas">Заказать картину</button>
				<div class="contacts-phone"><img src="<?php echo esc_url( get_template_directory_uri(), 'colinear' ) ?>/icons/phone.png" class="phone-img">
				<p class="phone-number">8 (909) 994-02-49</p></div>
			</div><!-- .site-contacts-->

			<?php if ( has_nav_menu( 'primary' ) ) : ?>
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="screen-reader-text"><?php esc_html_e( 'Primary Menu', 'colinear' ); ?></span></button>
				<?php
				// Primary navigation menu.
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
					) );
				?>
			</nav><!-- #site-navigation -->
			<?php endif; ?>
	
			<?php if ( get_header_image() ) : ?>
			<div class="header-image">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="">
				</a>
			</div><!-- .header-image -->
			<?php endif; // End header image check. ?>


		</div><!-- .header-info-container -->
		<div class="header-bottom-line">
			<div class="header-bottom-line-content">
				<div class="email-container">
					<img src="<?php echo esc_url( get_template_directory_uri(), 'colinear' ) ?>/icons/email.png" class="email-img">
					<p class="email">artdashevich@gmail.com</p>
				</div>
				<div class="social-container">
					<img src="<?php echo esc_url( get_template_directory_uri(), 'colinear' ) ?>/icons/facebook.png" class="social-icon facebook-img">
					<img src="<?php echo esc_url( get_template_directory_uri(), 'colinear' ) ?>/icons/vk.png" class="social-icon vk-img">
				</div>
			</div>
		</div><!-- .header-bottom-line-->

	</header><!-- #masthead -->

	<div id="content" class="site-content">